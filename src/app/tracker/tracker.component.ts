import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.css']
})

export class TrackerComponent implements OnInit{

  id: string;
  ids: string;
  origin: string;
  destination: string;
  amount: string;
  source_city: string;
  destination_city: string;
  pkg_weight : string;
  order_date : string;
  country_currency : string;
  current_pkg_stage: number;
  name: string;
  tempData : any;
  private _jsonURL = 'assets/exp.json';
  
  constructor( private router: ActivatedRoute, private route: Router, private http: HttpClient) {
  }
  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.id = JSON.parse(params["id"]);
      this.updateView();
    });
  }
  trackingId(id: any): void{
    let navigationExtras: NavigationExtras = {
      queryParams: {
          "id": JSON.stringify(id)
      }
    };
    this.route.navigate(['tracker'], navigationExtras);
  }
  public getJSON(): Observable<any> {
    return this.http.get(this._jsonURL);
  }

  
  updateView(){
    var parent = this;
    this.getJSON().subscribe(data => {
      parent.ids = data;
      this.tempData = data.find(x => x.trackid == this.id);
        parent.name = this.tempData.name;
        parent.amount = this.tempData.amount;
        parent.source_city = this.tempData.source_city;
        parent.destination_city = this.tempData.destination_city;
        parent.pkg_weight = this.tempData.pkg_weight;
        parent.country_currency = this.tempData.country_currency;
        parent.order_date = this.tempData.order_date;
        parent.current_pkg_stage = this.tempData.current_pkg_stage ? this.tempData.current_pkg_stage : '';
     });
     
  }
}
